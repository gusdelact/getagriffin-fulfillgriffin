package fulfillgriffin.cdk;

import software.amazon.awscdk.core.App;

import java.util.Arrays;

public class FulfillGriffinApp {
    public static void main(final String[] args) {

        App app = new App();
        new FulfillGriffinStack(app, "FulfillGriffinStack");
        app.synth();
    }
}

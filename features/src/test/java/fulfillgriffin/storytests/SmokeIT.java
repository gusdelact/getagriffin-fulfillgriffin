package fulfillgriffin.storytests;

import fulfillgriffin.cdk.FulfillGriffinStack;

import cucumber.api.java.Before;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static org.junit.Assert.*;

import software.amazon.awscdk.core.App;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
Verify that the CDK/CloudFormation deployment was successful.
*/
public class SmokeIT {
    private final static ObjectMapper JSON =
        new ObjectMapper().configure(SerializationFeature.INDENT_OUTPUT, true);

	private JsonNode actual;

	@When("I deploy the component in AWS")
    public void testStack() throws Exception {
        App app = new App();
        FulfillGriffinStack stack = new FulfillGriffinStack(app, "test");

        // synthesize the stack to a CloudFormation template and compare against
        // a checked-in JSON file.
        this.actual = JSON.valueToTree(app.synth().getStackArtifact(stack.getArtifactId()).getTemplate());
    }

	@Then("the JSON matches")
	public void verifyDeployment() throws Exception {
		assertEquals(new ObjectMapper().createObjectNode(), this.actual);
	}
}

package fulfillgriffin.storytests;

import fulfillgriffin.stub.*;

import cucumber.api.java.Before;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static org.junit.Assert.*;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.net.URL;

public class BasicIT {

	private String instance_griffinId = null;

	private String fulfillGriffinFactoryClassName = System.getenv("FULFILL_GRIFFIN_FACTORY_CLASS_NAME");
	private FulfillGriffinFactory fgFac;
	private FulfillGriffinStub fg;

	private String jdbcPort = System.getenv("FULFILL_GRIFFIN_MYSQL_HOST_PORT");
	private String jdbcUrl = "jdbc:mysql://localhost:" + this.jdbcPort + "/FulfillGriffin?connectTimeout=0&socketTimeout=0&autoReconnect=true";
	private String jdbcUser = System.getenv("FULFILL_GRIFFIN_MYSQL_USER");
	private String jdbcPass = System.getenv("FULFILL_GRIFFIN_MYSQL_PASSWORD");

	public BasicIT() {
		if (fulfillGriffinFactoryClassName == null) throw new Error(
			"Env variable FULFILL_GRIFFIN_FACTORY_CLASS_NAME not set");
		try {
			fgFac = (FulfillGriffinFactory)(Class.forName(fulfillGriffinFactoryClassName).getDeclaredConstructor().newInstance());
			fg = fgFac.createFulfillGriffin();
		} catch (Exception ex) {
			throw new RuntimeException("During startup", ex);
		}
		System.err.println("Constructed FulfillGriffin stub");

		if (this.jdbcPort == null) throw new RuntimeException("Env variable FULFILL_GRIFFIN_MYSQL_HOST_PORT not set");
		if (this.jdbcUser == null) throw new RuntimeException("Env variable FULFILL_GRIFFIN_MYSQL_USER not set");
		if (this.jdbcPass == null) throw new RuntimeException("Env variable FULFILL_GRIFFIN_MYSQL_PASSWORD not set");
	}


	/* Scenario Add To Inventory */

	@Given("that the database does not contain {string}")
	public void database_does_not_contain(String griffinId) throws Exception {
		purgeDb();
	}

	@When("I call AddToInventory for {string}")
	public void add_to_inventory(String griffinId) throws Exception {

		fg.addToInventory(griffinId);
		this.instance_griffinId = griffinId; // save for next step
	}

	@Then("a subsequent call to AddToInventory for {string} will return an error")
	public void does_not_return_an_error(String griffinId) throws Exception {

        try {
			System.err.println("2 About to call addToInventory(" + griffinId + ")");
			fg.addToInventory(griffinId);
			System.err.println("...returned from calling addToInventory");
		} catch (Exception ex) {
			System.err.println("Add to Inventory scenario passed");
			return;
		}
		throw new Exception("AddToInventory succeeded but should not have - griffin should be in inventory already");
	}

	/* Scenario Requisition */

	@Given("that the database contains only {string}")
	public void populate(String griffinId) throws Exception {
		purgeDb();
		fg.addToInventory(griffinId);
	}

	@When("I call RequisitionFromInventory with department number {int}")
	public void call_place_order(int deptNo) throws Exception {

		String griffinId = null;
		try {
			griffinId = fg.requisitionFromInventory(deptNo);
		} catch (Exception ex) {
			ex.printStackTrace(System.err);
			throw ex;
		}

		this.instance_griffinId = griffinId;
	}

	@Then("it returns GriffinID {string}")
	public void returns_a_griffin_id(String griffinId) throws Exception {
		if (! griffinId.equals(this.instance_griffinId)) throw new Exception(
			"Griffin Id returned is " + griffinId + " instead of " + this.instance_griffinId);
	}

	private void purgeDb() throws Exception {
		try {
			System.err.println("Getting connection to database...");
			Connection conn = DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPass);
			if (conn == null) throw new Exception("Unable to connect to FulfillGriffin database");
			Statement stmt = conn.createStatement();
			System.err.println("About to truncate FulfillGriffin...");
			stmt.executeUpdate("TRUNCATE TABLE FulfillGriffin");
			System.err.println("...truncated FulfillGriffin.");
		} catch (Exception ex) {
			ex.printStackTrace(System.err);
			throw ex;
		}
	}
}

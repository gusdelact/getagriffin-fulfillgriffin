use FulfillGriffin;

DROP TABLE IF EXISTS `FulfillGriffin`;
CREATE TABLE `FulfillGriffin` (
  `GriffinId`	varchar(255) NOT NULL,
  `DeptNo`		bigint(20),
  PRIMARY KEY (`GriffinId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

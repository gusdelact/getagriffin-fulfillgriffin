package fulfillgriffin.stub;
/* */
public interface FulfillGriffinFactory {
	FulfillGriffinStub createFulfillGriffin() throws Exception;
}

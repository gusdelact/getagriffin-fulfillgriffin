package fulfillgriffin.stub;

import cliffberg.utilities.HttpUtilities;

import com.google.gson.GsonBuilder;
import com.google.gson.Gson;

import java.net.URL;

public class FulfillGriffinFactoryLive implements FulfillGriffinFactory {

	public FulfillGriffinStub createFulfillGriffin() throws Exception {

		return new FulfillGriffinStub() {

			private String serviceURL;

			{
				this.serviceURL = System.getenv("FULFILL_GRIFFIN_SERVICE_URL");
				if (serviceURL == null) throw new RuntimeException(
					"FULFILL_GRIFFIN_SERVICE_URL not set in environment");
			}

			public String requisitionFromInventory(int deptNo) throws Exception {
				/* Call the FulfillGriffin microservice at the RequisitionFromInventory endpoint: */
				System.err.println("FULFILL_GRIFFIN_SERVICE_URL=" + this.serviceURL);
				System.err.println("Sending request: " + this.serviceURL + "/RequisitionFromInventory?DeptNo=" + deptNo);
				String jsonStr = HttpUtilities.getHttpResponseAsString(
					new URL(this.serviceURL + "/RequisitionFromInventory?DeptNo=" + deptNo));
				RequisitionResponse orderResponse = parseResponse(jsonStr);
				String griffinId = orderResponse.getGriffinId();
				if ((griffinId == null) || griffinId.equals("")) throw new RuntimeException(
					"Internal server error: empty griffin ID received from server");

				return griffinId;
			}

			public void addToInventory(String griffinId) throws Exception {
				/* Call the FulfillGriffin microservice at the CancelRequisition endpoint: */
		        URL url = new URL(this.serviceURL + "/AddToInventory?GriffinId=" + griffinId);
				HttpUtilities.getHttpResponseAsString(url);
			}

			public void cancelRequisition(String griffinId) throws Exception {
				URL url = new URL(this.serviceURL + "/CancelRequisition?GriffinId=" + griffinId);
				HttpUtilities.getHttpResponseAsString(url);
			}

			public int getNoOfGriffinsAvailable() throws Exception {
				String response = HttpUtilities.getHttpResponseAsString(
					new URL(this.serviceURL + "/GetGriffinsAvailCount"));
				int count = 0;
				try { count = Integer.parseInt(response); }
				catch (Exception ex) { throw new RuntimeException(ex); }
				return count;
			}

			public String ping() throws Exception {
				return HttpUtilities.getHttpResponseAsString(
					new URL(this.serviceURL + "/ping"));
			}
		};
	}

	static class RequisitionResponse {
		public String griffinId;
		public RequisitionResponse(String griffinId) { this.griffinId = griffinId; }
		public void setGriffinId(String griffinId) { this.griffinId = griffinId; }
		public String getGriffinId() { return this.griffinId; }
	}

	private RequisitionResponse parseResponse(String jsonStr) throws Exception {

		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		RequisitionResponse requisitionResponse;
		try {
			requisitionResponse = gson.fromJson(jsonStr, RequisitionResponse.class);
		} catch (Exception ex) {
			throw new RuntimeException("Ill-formatted JSON server response: " + jsonStr);
		}
		return requisitionResponse;
	}
}

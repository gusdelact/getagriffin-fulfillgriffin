package fulfillgriffin.stub;

import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Map;
import java.util.HashMap;

public class FulfillGriffinFactoryMock implements FulfillGriffinFactory {

	private SortedSet<String> availGriffins = new TreeSet<String>();

	public FulfillGriffinFactoryMock() {

		availGriffins.add("abc");
		availGriffins.add("def");
	}

	public FulfillGriffinStub createFulfillGriffin() {
		return new FulfillGriffinStub() {
			/* Mock implementations */

			public String requisitionFromInventory(int deptNo) throws Exception {
				if (availGriffins.size() == 0) throw new Exception("No griffins available");
				String griffinId = availGriffins.first();
				availGriffins.remove(griffinId);
				return griffinId;
			}

			public void addToInventory(String griffinId) throws Exception {
				try { availGriffins.add(griffinId); }
				catch (Exception ex) { throw new Exception("Unable to add griffin"); }
			}

			public void cancelRequisition(String griffinId) throws Exception {
				try { availGriffins.add(griffinId); }
				catch (Exception ex) { throw new Exception("Unable to add griffin"); }
			}

			public int getNoOfGriffinsAvailable() throws Exception {
				return availGriffins.size();
			}

			public String ping() throws Exception {
				return "ping";
			}
		};
	}
}
